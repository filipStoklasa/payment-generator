//
//  ContentView.swift
//  paymentGenerator
//
//  Created by Filip Stoklasa on 15/02/2020.
//  Copyright © 2020 Filip Stoklasa. All rights reserved.
//

import UIKit

class MainController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    var activeField: UITextField?
    var selectedCurrency: String!
    var formProcess = FormData()
    let currencyList = ["CZK","USD","EUR"]
    @IBOutlet weak var currencyField: UITextField!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createPickerView()
        createDateView()
        setDate(date: Date())
        txtView.delegate = self
        formProcess.setValue(typeOfInput:"currencyValue", value: currencyList[0])
        
        let tap = UITapGestureRecognizer(target:self.view,action:#selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        formProcess.setValue(typeOfInput:"messageValue", value:textView.text )
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        scrollView.isScrollEnabled = true
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)

        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets

        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        
        if activeField != nil
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        } else {
            scrollView.scrollRectToVisible(aRect, animated: true)
        }
    }

    @objc func keyboardWillDisappear(notification: NSNotification){
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        view.endEditing(true)
        scrollView.isScrollEnabled = false
        activeField = nil
    }
    
    func setDate (date:Date){
        let dateFormaterPrint = DateFormatter()
        dateFormaterPrint.dateFormat = "dd. MMM. yyyy"
        formProcess.setDate(date: date)
        dateField.text = dateFormaterPrint.string(from: date)
    }
    
    //PICKER VIEWS
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencyList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencyList[row]
          
    }
       
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        formProcess.setValue(typeOfInput: "currencyValue", value: currencyList[row])
        currencyField.text = currencyList[row]
    }
    
    func createCurrencyPickerView() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        currencyField.inputView = pickerView
     }
     
    func createDismissCurrencyPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
         
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        currencyField.inputAccessoryView = toolBar
    }
    
    func createPickerView(){
        createCurrencyPickerView()
        createDismissCurrencyPickerView()
        
    }
    
    func createDatePickerView() {
        let pickerView = UIDatePicker()
        pickerView.datePickerMode = .date
        pickerView.addTarget(self, action: #selector(pickDate(picker:)), for:.valueChanged)
        dateField.inputView = pickerView
    }
    
    @objc func pickDate(picker: UIDatePicker){
        setDate(date: picker.date)
    }
        
    func createDismissDatePickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        dateField.inputAccessoryView = toolBar
    }
    
       
    func createDateView(){
        createDatePickerView()
        createDismissDatePickerView()
    }
     
     @objc func action() {
        view.endEditing(true)
     }
    
    @IBAction func submitForm(_ sender: UIButton) {
         self.performSegue(withIdentifier:"goToResult", sender:self)
    }
    //STRINGINPUTS
    
    @IBAction func onInputChange(_ sender: UITextField) {
        formProcess.setValue(typeOfInput:sender.restorationIdentifier!, value: sender.text!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult" {
            let destinationVC = segue.destination as! ResultController
            let code = formProcess.processFormForQR()
            destinationVC.code = code
        }
    }
    
    @IBAction func onFocus(_ textField: UITextField) {
        activeField = textField
    }
}

