//
//  ContentView.swift
//  paymentGenerator
//
//  Created by Filip Stoklasa on 15/02/2020.
//  Copyright © 2020 Filip Stoklasa. All rights reserved.
//

import UIKit

class ResultController: UIViewController {
    var code = ""
    @IBOutlet weak var qrPlaceholder: UIImageView!
    @IBOutlet weak var qrCode: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        qrCode.text = code
        print(code)
        qrPlaceholder.image = generateQRCode(from: code)
    }
    
    func generateQRCode(from string: String) -> UIImage? {
          let data = string.data(using: String.Encoding.ascii)

          if let filter = CIFilter(name: "CIQRCodeGenerator") {
              filter.setValue(data, forKey: "inputMessage")
              let transform = CGAffineTransform(scaleX: 3, y: 3)

              if let output = filter.outputImage?.transformed(by: transform) {
                  return UIImage(ciImage: output)
              }
          }

          return nil
      }
}

