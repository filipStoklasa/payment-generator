//
//  FormData.swift
//  paymentGenerator
//
//  Created by Filip Stoklasa on 16/02/2020.
//  Copyright © 2020 Filip Stoklasa. All rights reserved.
//

import Foundation

class FormData {
    var formData:[String:String] = [
        "ibanValue" : "CZ6508000000192000145399",
        "amountValue" : "2599",
        "currencyValue" : "CZK",
        "dateValue" : "",
        "varValue" : "0407082392",
        "messageValue": "Uz jsi tady mel bejt ctyri minuty ty magore"
    ]
    var formFilled = false
    
    func setValue(typeOfInput:String, value:String){
        var formatedValue = value
        
        if(typeOfInput == "ibanValue" || typeOfInput == "messageValue"){
            formatedValue = value.folding(options: .diacriticInsensitive, locale: .current)
        }
        
        formData[typeOfInput] = formatedValue
    }
    
    func getFormData() -> [String:String] {
        return formData
    }
    
    func processFormForQR () -> String {
        return "SPD*1.0*ACC:\(formData["ibanValue"]!)*AM:\(formData["amountValue"]!)*CC:\(formData["currencyValue"]!)*DT:\(formData["dateValue"]!)*MSG:\(formData["messageValue"]!)*X-VS:\(formData["varValue"]!)"
    }
    
    func setDate(date:Date){
        let dateFormatterForm = DateFormatter()
        dateFormatterForm.dateFormat = "yyyyMMdd"
        formData["dateValue"] = dateFormatterForm.string(from: date)
    }
}
